# Carousel Scandiweb technical test

## Components definitions :

    - `Arrow which is useful to move to previous and next slide`
    - `Dot which represent diffrent slide with there number..by clicking on any number it would jump directly to that slide`
    - `Slide component is used for each individual slide`
    - `Slider is parent component which integrate all of above component to make complete functanality`

### Manual configuration without create-react-app

    - `Create a package.json by npm init -y`
    - `Add dependencies of react and react-dom`
    - `Add index.html`
    - `Add devDependencies of @babel/core @babel/preset-env @babel/preset-react webpack webpack-cli webpack-dev-server babel-loader html-webpack-plugin`
    - `Setup webpack.config.js with the configuration`
    - `Setup the script for run and build`

#### How to use

- `git clone scandiweb-technical-test`
- `cd scandiweb-technical-test`
- `npm i`
- `npm start`
